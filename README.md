# Suckless Builds - Flexipatch

![flexi](/uploads/e241080f71daca6f0558d9e7ce90ba99/flexi.png)

IF YOU USE THIS REPO PLEASE EDIT CONFIG.DEF.H IN DWM AND ST SO THAT THE INCLUDE LINES AT THE TOP OF THE FILES POINTS TO THE CORRECT LOCATION OF THE THEMES FOLDER ON YOUR SYSTEM

A repository for my builds of Suckless software.  All built with Flexipatch and cleaned up with Flexipatch Finaliser so that the config.h files are easier to use.

Flexipatch is the work of bakkeby and his git repos for dwm, st and flexipatch-finalizer can be found below:

https://github.com/bakkeby/dwm-flexipatch

https://github.com/bakkeby/st-flexipatch

https://github.com/bakkeby/flexipatch-finalizer

I've also included a dwmscripts folder that contains scripts for my status bar.  Use as you see fit and adjust to your setup.

There are two bar scripts, dwmbar and dwmbar2.  The first uses emoji from the Joy Pixels font so you will need to install libXft-bgra for this to work.  The second, dwmbar2, uses awesome fonts and the status2d patch to provide colour.
I've also configured dwm and st to use the Ubuntu Nerd font.

## DWM and ST

This is a heavily patched version of DWM that contains the following patches that I have explcitly enabled:

BAR_STATUS2D_PATCH

BAR_SYSTRAY_PATCH

BAR_STATUSALLMONS_PATCH

BAR_STATUSPADDING_PATCH

ATTACHASIDE_PATCH

AUTOSTART_PATCH

CYCLELAYOUTS_PATCH

RESTARTSIG_PATCH

ROTATESTACK_PATCH

SCRATCHPADS_PATCH

TAGOTHERMONITOR_PATCH
 
VANITYGAPS_PATCH 

ST has the following patches enabled:

ALPHA_PATCH

ANYSIZE_PATCH

CLIPBOARD_PATCH

FONT2_PATCH

OPENURLONCLICK_PATCH

SCROLLBACK_PATCH

SCROLLBACK_MOUSE_PATCH

SCROLLBACK_MOUSE_ALTSCREEN_PATCH

W3M_PATCH 

Feel free to use these builds.  I do not, however, offer or imply any form of support or ongoing maintenance.  And of course, you use them entirely at your own risk.

## License
The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here (https://opensource.org/licenses/MIT).



